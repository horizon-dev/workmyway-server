from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from smartcup_server import views
from smartcup_server.rpc import jsonrpc

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^rpc/', jsonrpc, name="jsonrpc"),
    url(r'^users/$', views.list_users),
    url(r'^data/$', views.data),
    url(r'^data/([^/]+)/$', views.display_data),
    url(r'^data/([^/]+)/export/$', views.export_data),
    url(r'^data/([^/]+)/counts/([^/]+)/([^/]+)/([^/]+)/([^/]+)/$', views.export_counts),
    url(r'^upload/accelerometer/', views.accelerometer_readings_upload)
]

urlpatterns += staticfiles_urlpatterns()
