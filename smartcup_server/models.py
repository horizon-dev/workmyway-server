from django.db import models
from django.contrib.auth.models import User

class AccelerometerReading(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    xVal = models.FloatField()
    yVal = models.FloatField()
    zVal = models.FloatField()
    lite = models.BooleanField(default=False)

class MotionReading(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    lite = models.BooleanField(default=False)

class StepReading(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    lite = models.BooleanField(default=False)

class TapReading(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    sign = models.TextField()
    tapType = models.TextField()
    lite = models.BooleanField(default=False)

class ConnectionStatus(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    connected = models.BooleanField()
    expected = models.BooleanField()
    lite = models.BooleanField(default=False)

class TrackingStatus(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    status = models.BooleanField()
    lite = models.BooleanField(default=False)

class LogMessage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    message = models.TextField()
    lite = models.BooleanField(default=False)

class SelfReport(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    start = models.DateTimeField()
    details = models.TextField()
    duration = models.IntegerField()
    active = models.BooleanField(default=True)
    lite = models.BooleanField(default=False)

class Alert(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    action = models.TextField()
    lite = models.BooleanField(default=False)

class ConfigurationChange(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    key = models.TextField()
    value = models.TextField()
    lite = models.BooleanField(default=False)

class Heartbeat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    deviceType = models.IntegerField()
    timestamp = models.DateTimeField(db_index=True)
    lite = models.BooleanField(default=False)

class DayComparisonReward(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    day = models.DateField()
    data = models.TextField()
    lite = models.BooleanField(default=False)

class GoalReward(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    appID = models.IntegerField()
    day = models.DateField()
    data = models.TextField()
    rewarded = models.BooleanField()
    lite = models.BooleanField(default=False)
