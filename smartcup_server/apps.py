from django.apps import AppConfig
from jsonrpcserver import config

class SmartcupServerConfig(AppConfig):
    config.schema_validation = False
    config.debug = False
    config.convert_camel_case = False
    config.log_requests = False
    name = 'smartcup_server'
