from datetime import (datetime, timedelta, timezone)

import struct

from bokeh.embed import file_html
from bokeh.layouts import (column, widgetbox)
from bokeh.models import (HoverTool, CustomJS)
from bokeh.models.widgets import CheckboxGroup
from bokeh.plotting import (figure, ColumnDataSource)
from bokeh.resources import CDN

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import connection
from django.http import (HttpResponse, StreamingHttpResponse)
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import numpy

import pandas
import pandas.io.sql as pandas_sql

from .models import (AccelerometerReading, StepReading, MotionReading)

def auth(username, password):
    user = authenticate(username=username, password=password)
    if user is None:
        raise PermissionDenied()
    return user

@csrf_exempt
def accelerometer_readings_upload(request):
    user = auth(request.GET.get('username'), request.GET.get('password'))
    lite = request.GET.get('lite', default='0') == '1'
    i = struct.iter_unpack('>BIfff', request.body)
    insert = []
    for data_record in i:
        reading = AccelerometerReading()
        reading.user = user
        reading.appID = 0
        reading.deviceType = data_record[0]
        reading.timestamp = datetime.fromtimestamp(data_record[1], timezone.utc)
        reading.xVal = data_record[2]
        reading.yVal = data_record[3]
        reading.zVal = data_record[4]
        reading.lite = lite
        insert.append(reading)
    AccelerometerReading.objects.bulk_create(insert)
    return HttpResponse(status=200)

def index(request):
    return render(request, 'index.html')


def data(request):
    return render(request, 'data.html', {
        "users": User.objects.all(),
    })


def get_range(request):
    start = request.GET.get('from', '')
    if start.isdigit():
        start = datetime.fromtimestamp(int(start) / 1000, timezone.utc)
    else:
        start = datetime.fromtimestamp(0, timezone.utc)
    end = request.GET.get('to', '')
    if end.isdigit():
        end = datetime.fromtimestamp(int(end) / 1000, timezone.utc)
    else:
        end = datetime.now(timezone.utc)
    return (start, end)


def list_users(request):
    users = User.objects.all()
    def responder():
        for user in users:
            yield str(user.id) + ':' + user.username + '\n'
    response = StreamingHttpResponse(responder(), content_type='text/plain')
    return response


def display_data(request, user_id):
    start, end = get_range(request)
    acc_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        '(sqrt(power(x, 2) + power(y, 2) + power(z, 2))) as magnitude, '
        'to_char(timestamp, \'DD-MM HH24:MI:SS.MS\') as ftimestamp '
        'FROM ' + AccelerometerReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'])
    step_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        'to_char(timestamp, \'DD-MM HH24:MI:SS.MS\') as ftimestamp '
        'FROM ' + StepReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'])
    motion_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        'to_char(timestamp, \'DD-MM HH24:MI:SS.MS\') as ftimestamp '
        'FROM ' + MotionReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'])
    mean = numpy.mean(acc_data['magnitude'])
    plot = figure(
        title="SmartCup Accelerometer Data",
        x_axis_label='Time',
        y_axis_label='Magnitude',
        webgl=False,
        x_axis_type='datetime',
        responsive=True,
        tools='hover,pan,box_zoom,wheel_zoom,save,reset')
    plot.sizing_mode = 'stretch_both'
    plot.select(dict(type=HoverTool)).tooltips = \
            [('time', '@ftimestamp'), ('y', '$y')]
    line = plot.line(
        'timestamp', 'magnitude',
        source=ColumnDataSource(ColumnDataSource.from_df(acc_data)),
        line_width=1, color='#470A64', legend="Magnitude")
    steps = plot.rect(
        step_data['timestamp'], mean + 0.25,
        source=ColumnDataSource(ColumnDataSource.from_df(step_data)),
        width=2, height=0.5, width_units='screen',
        color="#974E08", alpha=0.5, legend="Step")
    motions = plot.rect(
        motion_data['timestamp'], mean - 0.25,
        source=ColumnDataSource(ColumnDataSource.from_df(motion_data)),
        width=2, height=0.5, width_units='screen',
        color="#055A5A", alpha=0.5, legend="Significant Motion")

    checkbox = CheckboxGroup(
        labels=["Magnitude", "Steps", "Motion"], active=[0, 1, 2])
    checkbox.callback = CustomJS(
        args=dict(line=line, steps=steps, motions=motions),
        code="""
        line.visible = false;
        steps.visible = false;
        motions.visible = false;
        for (i in cb_obj.active) {
            if (cb_obj.active[i] == 0) {
                line.visible = true;
            } else if (cb_obj.active[i] == 1) {
                steps.visible = true;
            } else if (cb_obj.active[i] == 2) {
                motions.visible = true;
            }
        }
        """)

    layout = column(plot, widgetbox(checkbox),
                    sizing_mode='stretch_both', responsive=True)

    return HttpResponse(file_html(layout, CDN, "SmartCup Accelerometer Data"))

def export_data(request, user_id):
    start, end = get_range(request)
    acc_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        '(sqrt(power(x, 2) + power(y, 2) + power(z, 2))) as magnitude, '
        'to_char(timestamp, \'DD-MM-YYYY HH24:MI:SS.MS\') as ftimestamp '
        'FROM ' + AccelerometerReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'],
        index_col='timestamp')
    step_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        'to_char(timestamp, \'DD-MM-YYYY HH24:MI:SS.MS\') as ftimestamp, '
        '\'1\' as step '
        'FROM ' + StepReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'],
        index_col='timestamp')
    motion_data = pandas_sql.read_sql(
        'SELECT timestamp, '
        'to_char(timestamp, \'DD-MM-YYYY HH24:MI:SS.MS\') as ftimestamp, '
        '\'1\' as motion '
        'FROM ' + MotionReading._meta.db_table + ' '
        'WHERE timestamp >= %s AND timestamp <= %s '
        'AND user_id = %s ORDER BY timestamp ASC',
        connection,
        params=[start, end, user_id],
        parse_dates=['timestamp'],
        index_col='timestamp')
    #all_data = pandas.concat([acc_data, step_data, motion_data]).sort_index()
    im_data = pandas.merge(
        acc_data, step_data, how='outer', on=None, left_on=None, right_on=None,
        left_index=False, right_index=False, sort=True,
        suffixes=('_x', '_y'), copy=True, indicator=False)
    all_data = pandas.merge(
        im_data, motion_data, how='outer', on=None, left_on=None, right_on=None,
        left_index=False, right_index=False, sort=True,
        suffixes=('_x', '_y'), copy=True, indicator=False)
    def responder():
        last_magnitude = '0'
        yield 'Timestamp,Magnitude,Motion,Step\n'
        for _, row in all_data.iterrows():
            if pandas.isnull(row['magnitude']):
                magnitude = ''#last_magnitude
            else:
                magnitude = str(row['magnitude'])
                last_magnitude = magnitude
            step = '0' if pandas.isnull(row['step']) \
                    else '1'
            motion = '0' if pandas.isnull(row['motion']) \
                    else '1'
            yield row['ftimestamp'] + ',' + magnitude + ',' \
                    + motion + ',' + step + '\n'
    response = StreamingHttpResponse(responder(), content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=smartcup.csv'
    return response

def export_counts(request, user_id, year, month, day, type):
    start = datetime(int(year), int(month), int(day), 0, 0, 0, 0, timezone.utc)
    end = start + timedelta(1)
    counts = None
    if type == 'step':
        counts = StepReading.objects.filter(timestamp__gt=start, timestamp__lt=end, user_id=user_id)
    else:
        counts = MotionReading.objects.filter(timestamp__gt=start, timestamp__lt=end, user_id=user_id)
    def responder():
        for count in counts:
            yield count.timestamp.strftime('%H:%M:%S.%f') + '\n'
    response = StreamingHttpResponse(responder(), content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=counts.csv'
    return response
