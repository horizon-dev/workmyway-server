from django.contrib import admin

from .models import (
    AccelerometerReading,
    MotionReading,
    StepReading,
    TapReading,
    ConnectionStatus,
    TrackingStatus,
    LogMessage,
    SelfReport,
    Alert,
    ConfigurationChange,
    Heartbeat,
    DayComparisonReward,
    GoalReward
)

admin.site.register(AccelerometerReading)
admin.site.register(MotionReading)
admin.site.register(StepReading)
admin.site.register(TapReading)
admin.site.register(ConnectionStatus)
admin.site.register(TrackingStatus)
admin.site.register(LogMessage)
admin.site.register(SelfReport)
admin.site.register(Alert)
admin.site.register(ConfigurationChange)
admin.site.register(Heartbeat)
admin.site.register(DayComparisonReward)
admin.site.register(GoalReward)
