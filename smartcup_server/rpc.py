from datetime import (datetime, timedelta, timezone)

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from jsonrpcserver import methods

from .models import (
    MotionReading,
    StepReading,
    TapReading,
    ConnectionStatus,
    TrackingStatus,
    LogMessage,
    SelfReport,
    Alert,
    ConfigurationChange,
    Heartbeat,
    DayComparisonReward,
    GoalReward
)


def auth(username, password):
    user = authenticate(username=username, password=password)
    if user is None:
        raise PermissionDenied()
    return user


@methods.add
def sign_in(username, password, imei):
    if User.objects.filter(username=username).exists():
        user = authenticate(username=username, password=password)
        if user is None:
            return {'error': 'invalid_password'}
        else:
            return {}
    else:
        user = User.objects.create_user(username, imei, password)
        user.save()
        return {}


@methods.add
def save_motion_readings(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        reading = MotionReading()
        reading.user = user
        reading.appID = record['id']
        reading.deviceType = record['device_type']
        reading.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        reading.lite = lite
        insert.append(reading)
    MotionReading.objects.bulk_create(insert)
    return None


@methods.add
def save_step_readings(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        reading = StepReading()
        reading.user = user
        reading.appID = record['id']
        reading.deviceType = record['device_type']
        reading.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        reading.lite = lite

        if reading.timestamp > (datetime.now(timezone.utc) + timedelta(hours=1)):
            print("ERROR")
            print(datetime.now())
            print(user.id)
            print(record['timestamp'])
            print(reading.timestamp)

        insert.append(reading)
    StepReading.objects.bulk_create(insert)
    return None


@methods.add
def save_tracking_statuses(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        status = TrackingStatus()
        status.user = user
        status.appID = record['id']
        status.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        status.status = record['status']
        status.lite = lite
        insert.append(status)
    TrackingStatus.objects.bulk_create(insert)
    return None


@methods.add
def save_tap_readings(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        reading = TapReading()
        reading.user = user
        reading.appID = record['id']
        reading.deviceType = record['device_type']
        reading.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        reading.sign = record['sign']
        reading.tapType = record['tap_type']
        reading.lite = lite
        insert.append(reading)
    TapReading.objects.bulk_create(insert)
    return None


@methods.add
def save_connection_statuses(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        status = ConnectionStatus()
        status.user = user
        status.appID = record['id']
        status.deviceType = record['device_type']
        status.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        status.connected = record['connected']
        status.expected = record['expected']
        status.lite = lite
        insert.append(status)
    ConnectionStatus.objects.bulk_create(insert)
    return None


@methods.add
def save_log_messages(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        msg = LogMessage()
        msg.user = user
        msg.appID = record['id']
        msg.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        msg.message = record['message']
        msg.lite = lite
        insert.append(msg)
    LogMessage.objects.bulk_create(insert)
    return None


@methods.add
def save_self_reports(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        report = SelfReport()
        report.user = user
        report.appID = record['id']
        report.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        report.start = datetime.fromtimestamp(record['start'] / 1000, timezone.utc)
        report.details = record['details']
        report.duration = record['duration']
        report.active = record['active']
        report.lite = lite
        insert.append(report)
    SelfReport.objects.bulk_create(insert)
    return None


@methods.add
def save_alerts(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        alert = Alert()
        alert.user = user
        alert.appID = record['id']
        alert.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        alert.action = record['action']
        alert.lite = lite
        insert.append(alert)
    Alert.objects.bulk_create(insert)
    return None


@methods.add
def save_configuration_changes(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        change = ConfigurationChange()
        change.user = user
        change.appID = record['id']
        change.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        change.key = record['key']
        change.value = record['value']
        change.lite = lite
        insert.append(change)
    ConfigurationChange.objects.bulk_create(insert)
    return None


@methods.add
def save_heartbeats(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        heartbeat = Heartbeat()
        heartbeat.user = user
        heartbeat.appID = record['id']
        heartbeat.deviceType = record['device_type']
        heartbeat.timestamp = datetime.fromtimestamp(record['timestamp'] / 1000, timezone.utc)
        heartbeat.lite = lite
        insert.append(heartbeat)
    Heartbeat.objects.bulk_create(insert)
    return None


@methods.add
def save_day_comparison_rewards(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        reward = DayComparisonReward()
        reward.user = user
        reward.appID = record['id']
        reward.day = datetime.strptime(record['day'], '%d/%m/%Y').date()
        reward.data = record['data']
        reward.lite = lite
        insert.append(reward)
    DayComparisonReward.objects.bulk_create(insert)
    return None


@methods.add
def save_goal_rewards(username, password, data, lite=False):
    user = auth(username, password)
    insert = []
    for record in data:
        reward = GoalReward()
        reward.user = user
        reward.appID = record['id']
        reward.day = datetime.strptime(record['day'], '%d/%m/%Y').date()
        reward.data = record['data']
        reward.rewarded = record['rewarded']
        reward.lite = lite
        insert.append(reward)
    GoalReward.objects.bulk_create(insert)
    return None


@csrf_exempt
def jsonrpc(request):
    response = methods.dispatch(request.body.decode())
    return JsonResponse(response, status=response.http_status)
